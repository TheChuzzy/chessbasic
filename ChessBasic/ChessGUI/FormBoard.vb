﻿Public Class FormBoard
    Private Squares(7, 7) As Panel
    Private selectedSquare As Panel
    Private highlightedMoves As List(Of ChessBasic.Move)
    Private game As New ChessBasic.ChessGame

    Const TILE_SIZE As Integer = 50
    Private pieces As New Dictionary(Of Char, Bitmap) From
    {
        {"P"c, My.Resources.wp},
        {"N"c, My.Resources.wn},
        {"B"c, My.Resources.wb},
        {"R"c, My.Resources.wr},
        {"Q"c, My.Resources.wq},
        {"K"c, My.Resources.wk},
        {"p"c, My.Resources.bp},
        {"n"c, My.Resources.bn},
        {"b"c, My.Resources.bb},
        {"r"c, My.Resources.br},
        {"q"c, My.Resources.bq},
        {"k"c, My.Resources.bk}
    }
    Private rng As New Random
    Private lightColor As Color = Color.FromArgb(240, 217, 181)
    Private darkColor As Color = Color.FromArgb(181, 136, 99)

    Private Function SquareToPanel(square As ChessBasic.Square) As Panel
        Return Squares(square.FileIndex, square.RankIndex)
    End Function

    Private Function PanelToSquare(panel As Panel) As ChessBasic.Square
        Return game.Board("abcdefgh".IndexOf(panel.Name(0)), "12345678".IndexOf(panel.Name(1)))
    End Function

    Private Sub ClickPanel(sender As Object, e As EventArgs)
        Dim panel As Panel = CType(sender, Panel)
        If selectedSquare Is Nothing AndAlso PanelToSquare(panel).Piece IsNot Nothing Then
            If PanelToSquare(panel).Piece.OppositeOwner = game.CurrentTurn Then Return
            selectedSquare = panel
            ' Set background alpha to 50%
            selectedSquare.BackColor = Color.FromArgb(127, selectedSquare.BackColor)
            ' Generate legal moves for this piece
            highlightedMoves = PanelToSquare(selectedSquare).Piece.GenerateLegalMoves().ToList
            For Each legalMove In highlightedMoves
                SquareToPanel(legalMove.ToSquare).BackColor = Color.FromArgb(127, selectedSquare.BackColor)
            Next
            Label1.Text = "You selected " & PanelToSquare(selectedSquare).Piece.ToString
        ElseIf selectedSquare IsNot Nothing Then
            If Not panel.Equals(selectedSquare) Then
                Try
                    Dim selectedMove = highlightedMoves.Find(Function(move As ChessBasic.Move)
                                                                 Return move.ToSquare.Equals(PanelToSquare(panel))
                                                             End Function)
                    If selectedMove Is Nothing Then
                        ' Illegal move
                        Exit Try
                    Else
                        ' If the selected move involves a pawn promotion
                        If (selectedMove.Flags And ChessBasic.MoveFlag.Promotion) <> 0 Then
                            Dim promotionForm As New FormPromotion(selectedMove.ToSquare.ToString, game.CurrentTurn = ChessBasic.Color.Black)
                            promotionForm.ShowDialog()
                            Dim selectedPromotion As ChessBasic.Promotion = promotionForm.SelectedPromotion
                            selectedMove = New ChessBasic.Move(selectedMove, selectedPromotion)
                        End If
                        game.MakeMove(selectedMove)
                        Label1.Text = "You moved " & selectedMove.ToString
                    End If
                Catch ex As ChessBasic.InvalidChessMoveException
                End Try
            End If
            selectedSquare = Nothing
            UnhighlightPanels()
            UpdateBoard()
            If game.Checkmate Then
                LblCheck.Text = "Checkmate!"
            ElseIf game.Stalemate Then
                LblCheck.Text = "Stalemate!"
            ElseIf game.Check Then
                LblCheck.Text = "Check!"
            Else
                LblCheck.Text = String.Empty
            End If
        End If
        LblCurrentTurn.Text = "Game.CurrentTurn: " & game.CurrentTurn.ToString
    End Sub

    Private Sub HoverOverPanel(sender As Object, e As EventArgs)
        AttackedByTxtBox.Clear()
        Dim panel As Panel = CType(sender, Panel)
        Dim square As ChessBasic.Square = PanelToSquare(panel)

        AttackedByLbl.Text = "Pieces attacking " & square.ToString
        For Each attacker In square.AttackedBy
            AttackedByTxtBox.Text &= attacker.ToString & vbCrLf
        Next
        If square.Piece IsNot Nothing Then
            AttackedByTxtBox.Text &= "Pin direction: " & square.Piece.PinDirection.ToString & vbCrLf
        End If
        If square.ThreatToKing Then
            AttackedByTxtBox.Text &= square.ToString & " is a threat to the king!"
        End If
    End Sub

    Private Sub UnhighlightPanels()
        For i = 0 To 7
            For j = 0 To 7
                If i Mod 2 = 1 Then
                    Squares(j, i).BackColor = If(j Mod 2 <> 0, darkColor, lightColor)
                Else
                    Squares(j, i).BackColor = If(j Mod 2 <> 0, lightColor, darkColor)
                End If
            Next
        Next
    End Sub

    Private Sub UpdateBoard()
        For rank = 0 To 7
            For file = 0 To 7
                Squares(file, rank).BackgroundImage = If(game.Board(file, rank).Piece IsNot Nothing,
                                                         pieces(game.Board(file, rank).Piece.FENChar),
                                                         Nothing)
            Next
        Next
    End Sub

    Private Sub FormBoard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ChessBoardPanel.Size = New Size(TILE_SIZE * 8, TILE_SIZE * 8)
        'Size = ChessBoardPanel.Size + New Size(50, 65)
        For i = 0 To 7
            For j = 0 To 7
                Dim newPanel As New Panel With {
                    .Size = New Size(TILE_SIZE, TILE_SIZE),
                    .Location = New Point(TILE_SIZE * j, TILE_SIZE * 7 - TILE_SIZE * i),
                    .BackgroundImageLayout = ImageLayout.Stretch,
                    .Name = "abcdefgh"(j) + (i + 1).ToString
                }
                ChessBoardPanel.Controls.Add(newPanel)
                AddHandler newPanel.MouseClick, AddressOf ClickPanel
                AddHandler newPanel.MouseHover, AddressOf HoverOverPanel

                If i Mod 2 = 1 Then
                    newPanel.BackColor = If(j Mod 2 <> 0, darkColor, lightColor)
                Else
                    newPanel.BackColor = If(j Mod 2 <> 0, lightColor, darkColor)
                End If

                Squares(j, i) = newPanel
            Next
        Next
        UpdateBoard()
    End Sub

End Class