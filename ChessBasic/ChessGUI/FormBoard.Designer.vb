﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormBoard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ChessBoardPanel = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblCurrentTurn = New System.Windows.Forms.Label()
        Me.AttackedByTxtBox = New System.Windows.Forms.RichTextBox()
        Me.AttackedByLbl = New System.Windows.Forms.Label()
        Me.LblCheck = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ChessBoardPanel
        '
        Me.ChessBoardPanel.BackColor = System.Drawing.Color.Lime
        Me.ChessBoardPanel.Location = New System.Drawing.Point(12, 12)
        Me.ChessBoardPanel.Name = "ChessBoardPanel"
        Me.ChessBoardPanel.Size = New System.Drawing.Size(400, 400)
        Me.ChessBoardPanel.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(419, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Label1"
        '
        'LblCurrentTurn
        '
        Me.LblCurrentTurn.AutoSize = True
        Me.LblCurrentTurn.Location = New System.Drawing.Point(503, 395)
        Me.LblCurrentTurn.Name = "LblCurrentTurn"
        Me.LblCurrentTurn.Size = New System.Drawing.Size(143, 15)
        Me.LblCurrentTurn.TabIndex = 3
        Me.LblCurrentTurn.Text = "Game.CurrentTurn: White"
        '
        'AttackedByTxtBox
        '
        Me.AttackedByTxtBox.Location = New System.Drawing.Point(422, 155)
        Me.AttackedByTxtBox.Name = "AttackedByTxtBox"
        Me.AttackedByTxtBox.ReadOnly = True
        Me.AttackedByTxtBox.Size = New System.Drawing.Size(293, 96)
        Me.AttackedByTxtBox.TabIndex = 4
        Me.AttackedByTxtBox.Text = ""
        '
        'AttackedByLbl
        '
        Me.AttackedByLbl.AutoSize = True
        Me.AttackedByLbl.Location = New System.Drawing.Point(419, 137)
        Me.AttackedByLbl.Name = "AttackedByLbl"
        Me.AttackedByLbl.Size = New System.Drawing.Size(92, 15)
        Me.AttackedByLbl.TabIndex = 5
        Me.AttackedByLbl.Text = "Pieces attacking"
        '
        'LblCheck
        '
        Me.LblCheck.AutoSize = True
        Me.LblCheck.Font = New System.Drawing.Font("Segoe UI", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCheck.ForeColor = System.Drawing.Color.Red
        Me.LblCheck.Location = New System.Drawing.Point(416, 50)
        Me.LblCheck.Name = "LblCheck"
        Me.LblCheck.Size = New System.Drawing.Size(0, 32)
        Me.LblCheck.TabIndex = 6
        '
        'FormBoard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 423)
        Me.Controls.Add(Me.LblCheck)
        Me.Controls.Add(Me.AttackedByLbl)
        Me.Controls.Add(Me.AttackedByTxtBox)
        Me.Controls.Add(Me.LblCurrentTurn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ChessBoardPanel)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FormBoard"
        Me.Text = "FormBoard"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ChessBoardPanel As Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblCurrentTurn As Label
    Friend WithEvents AttackedByTxtBox As System.Windows.Forms.RichTextBox
    Friend WithEvents AttackedByLbl As System.Windows.Forms.Label
    Friend WithEvents LblCheck As System.Windows.Forms.Label
End Class
