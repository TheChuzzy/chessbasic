﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPromotion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnKnight = New System.Windows.Forms.Button()
        Me.BtnBishop = New System.Windows.Forms.Button()
        Me.BtnRook = New System.Windows.Forms.Button()
        Me.BtnQueen = New System.Windows.Forms.Button()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnKnight
        '
        Me.BtnKnight.Image = Global.ChessGUI.My.Resources.Resources.wn
        Me.BtnKnight.Location = New System.Drawing.Point(15, 15)
        Me.BtnKnight.Name = "BtnKnight"
        Me.BtnKnight.Size = New System.Drawing.Size(70, 69)
        Me.BtnKnight.TabIndex = 0
        Me.BtnKnight.UseVisualStyleBackColor = True
        '
        'BtnBishop
        '
        Me.BtnBishop.Image = Global.ChessGUI.My.Resources.Resources.wb
        Me.BtnBishop.Location = New System.Drawing.Point(92, 15)
        Me.BtnBishop.Name = "BtnBishop"
        Me.BtnBishop.Size = New System.Drawing.Size(70, 69)
        Me.BtnBishop.TabIndex = 1
        Me.BtnBishop.UseVisualStyleBackColor = True
        '
        'BtnRook
        '
        Me.BtnRook.Image = Global.ChessGUI.My.Resources.Resources.wr
        Me.BtnRook.Location = New System.Drawing.Point(169, 15)
        Me.BtnRook.Name = "BtnRook"
        Me.BtnRook.Size = New System.Drawing.Size(70, 69)
        Me.BtnRook.TabIndex = 2
        Me.BtnRook.UseVisualStyleBackColor = True
        '
        'BtnQueen
        '
        Me.BtnQueen.Image = Global.ChessGUI.My.Resources.Resources.wq
        Me.BtnQueen.Location = New System.Drawing.Point(246, 15)
        Me.BtnQueen.Name = "BtnQueen"
        Me.BtnQueen.Size = New System.Drawing.Size(70, 69)
        Me.BtnQueen.TabIndex = 3
        Me.BtnQueen.UseVisualStyleBackColor = True
        '
        'Cancel
        '
        Me.Cancel.Location = New System.Drawing.Point(15, 90)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(301, 27)
        Me.Cancel.TabIndex = 4
        Me.Cancel.Text = "Cancel"
        Me.Cancel.UseVisualStyleBackColor = True
        '
        'FormPromotion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(330, 129)
        Me.ControlBox = False
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.BtnQueen)
        Me.Controls.Add(Me.BtnRook)
        Me.Controls.Add(Me.BtnBishop)
        Me.Controls.Add(Me.BtnKnight)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(346, 168)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(346, 168)
        Me.Name = "FormPromotion"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "FormPromotion"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BtnKnight As Button
    Friend WithEvents BtnBishop As Button
    Friend WithEvents BtnRook As Button
    Friend WithEvents BtnQueen As Button
    Friend WithEvents Cancel As Button
End Class
