﻿Imports ChessBasic

Public Class FormPromotion

    Public SelectedPromotion As Promotion = Promotion.None

    Public Sub New(ByVal squareName As String, ByVal showBlackPieces As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Text = "Promoting " & squareName & " pawn"

        If showBlackPieces Then
            BtnKnight.Image = My.Resources.bn
            BtnBishop.Image = My.Resources.bb
            BtnRook.Image = My.Resources.br
            BtnQueen.Image = My.Resources.bq
        End If
    End Sub

    Private Sub BtnKnight_Click(sender As Object, e As EventArgs) Handles BtnKnight.Click
        SelectedPromotion = Promotion.Knight
        Close()
    End Sub

    Private Sub BtnBishop_Click(sender As Object, e As EventArgs) Handles BtnBishop.Click
        SelectedPromotion = Promotion.Bishop
        Close()
    End Sub

    Private Sub BtnRook_Click(sender As Object, e As EventArgs) Handles BtnRook.Click
        SelectedPromotion = Promotion.Rook
        Close()
    End Sub

    Private Sub BtnQueen_Click(sender As Object, e As EventArgs) Handles BtnQueen.Click
        SelectedPromotion = Promotion.Queen
        Close()
    End Sub

    Private Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click
        SelectedPromotion = Promotion.None
        Close()
    End Sub
End Class