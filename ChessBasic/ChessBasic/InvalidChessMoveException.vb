﻿Imports System.Runtime.Serialization
Imports System.Security

''' <summary>
''' The exception that occurs when a given chess move is invalid.
''' </summary>
<Serializable>
Public Class InvalidChessMoveException
    Inherits ChessException

    Public Sub New()
        Me.New("The chess move is invalid.")
    End Sub

    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(message As String, inner As Exception)
        MyBase.New(message, inner)
    End Sub

    <SecuritySafeCritical()>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class