﻿Option Strict On

Imports System.Collections.ObjectModel

Public Enum CastlingRights
    None
    KingsideOnly
    QueensideOnly
    Both
End Enum

Public Enum Color
    None
    White
    Black
    Both
End Enum

Public Enum Promotion
    None
    Knight
    Bishop
    Rook
    Queen
End Enum

<Flags>
Public Enum MoveFlag
    None = 0
    Capture = 1
    EnPassantCapture = 2
    PawnPushTwoSquares = 4
    Promotion = 8
    CastleKingside = 16
    CastleQueenside = 32
End Enum

<Flags>
Public Enum DirectionFlag
    None = 0
    North = 1
    East = 2
    South = 4
    West = 8
    Northwest = 16
    Northeast = 32
    Southwest = 64
    Southeast = 128
End Enum

Public Class ChessGame
    Public Shared ReadOnly Property StartingPosition As Dictionary(Of String, Char)
        Get
            Return New Dictionary(Of String, Char) From {
            {"a1", "R"c},
            {"b1", "N"c},
            {"c1", "B"c},
            {"d1", "Q"c},
            {"e1", "K"c},
            {"f1", "B"c},
            {"g1", "N"c},
            {"h1", "R"c},
            {"a2", "P"c},
            {"b2", "P"c},
            {"c2", "P"c},
            {"d2", "P"c},
            {"e2", "P"c},
            {"f2", "P"c},
            {"g2", "P"c},
            {"h2", "P"c},
            {"a7", "p"c},
            {"b7", "p"c},
            {"c7", "p"c},
            {"d7", "p"c},
            {"e7", "p"c},
            {"f7", "p"c},
            {"g7", "p"c},
            {"h7", "p"c},
            {"a8", "r"c},
            {"b8", "n"c},
            {"c8", "b"c},
            {"d8", "q"c},
            {"e8", "k"c},
            {"f8", "b"c},
            {"g8", "n"c},
            {"h8", "r"c}
        }
        End Get
    End Property

    Public Board(7, 7) As Square

    Friend WhiteKing, BlackKing As King

    Public EnPassantSquare As Square

    Public WhiteCastlingRights, BlackCastlingRights As CastlingRights

    Private currentTurnValue As Color = Color.White
    Public Property CurrentTurn As Color
        Get
            Return currentTurnValue
        End Get
        Private Set(ByVal value As Color)
            currentTurnValue = value
        End Set
    End Property

    Private checkValue As Boolean
    Public Property Check() As Boolean
        Get
            Return checkValue
        End Get
        Private Set(ByVal value As Boolean)
            checkValue = value
        End Set
    End Property

    Private checkmateValue As Boolean
    Public Property Checkmate() As Boolean
        Get
            Return checkmateValue
        End Get
        Private Set(ByVal value As Boolean)
            checkmateValue = value
        End Set
    End Property

    Private stalemateValue As Boolean
    Public Property Stalemate() As Boolean
        Get
            Return stalemateValue
        End Get
        Private Set(ByVal value As Boolean)
            stalemateValue = value
        End Set
    End Property

    Public ReadOnly Property WhitePieces As Piece()
        Get
            ' TODO: Cache this result and update as necessary rather than calculate it when needed
            Return (From square In Board.Cast(Of Square)() Where square.Piece IsNot Nothing AndAlso square.Piece.Owner = Color.White Select square.Piece).ToArray
        End Get
    End Property

    Public ReadOnly Property BlackPieces As Piece()
        Get
            ' TODO: Cache this result and update as necessary rather than calculate it when needed
            Return (From square In Board.Cast(Of Square)() Where square.Piece IsNot Nothing AndAlso square.Piece.Owner = Color.Black Select square.Piece).ToArray
        End Get
    End Property

    ''' <summary>
    ''' Calculates and returns a read-only list of legal moves.
    ''' </summary>
    Public ReadOnly Property LegalMoves As ReadOnlyCollection(Of Move)
        Get
            Dim moves As New List(Of Move)
            Dim myPieces As IEnumerable(Of Piece) = If(CurrentTurn = Color.White, WhitePieces, BlackPieces)
            For Each piece In myPieces
                moves.AddRange(piece.GenerateLegalMoves())
            Next
            Return New ReadOnlyCollection(Of Move)(moves)
        End Get
    End Property

    ''' <summary>
    ''' Recalculates the attacked squares.
    ''' </summary>
    Public Sub RecalculateAttackedSquares()
        ' Reset square attacked status.
        For i = 0 To 7
            For j = 0 To 7
                Board(j, i).AttackedBy.Clear()
            Next
        Next

        ' Iterate through each piece on the board
        For Each piece In WhitePieces.Concat(BlackPieces)
            ' Iterate through each move for this piece
            For Each move In piece.GenerateAttackingMoves()
                ' Add this piece to the square's attackers
                move.ToSquare.AttackedBy.Add(piece)
            Next
        Next
    End Sub

    ''' <summary>
    ''' Updates the attacked status of only the necessary squares.
    ''' </summary>
    ''' <param name="move">The move that was just made.</param>
    Public Sub UpdateAttackedSquares(ByVal move As Move)
        ' TODO: Fix UpdateAttackedSquares
        ' If a pawn just moved
        If move.ToSquare.Piece.GetType = GetType(Pawn) Then
            Dim prevRank As Integer = If(move.ToSquare.Piece.Owner = Color.White, -1, 1)
            Dim westPreviouslyAttackedSquare, eastPreviouslyAttackedSquare As Square
            ' Remove the pawn from the previously attacked AttackedBy hashset
            If (move.Flags And MoveFlag.PawnPushTwoSquares) <> 0 Then
                With move.ToSquare
                    Try
                        westPreviouslyAttackedSquare = Board(.FileIndex - 1, .RankIndex + prevRank)
                    Catch ex As IndexOutOfRangeException
                        westPreviouslyAttackedSquare = Nothing
                    End Try
                    Try
                        eastPreviouslyAttackedSquare = Board(.FileIndex + 1, .RankIndex + prevRank)
                    Catch ex As Exception
                        eastPreviouslyAttackedSquare = Nothing
                    End Try
                End With
            Else
                With move.ToSquare
                    Try
                        westPreviouslyAttackedSquare = Board(.FileIndex - 1, .RankIndex)
                    Catch ex As IndexOutOfRangeException
                        westPreviouslyAttackedSquare = Nothing
                    End Try
                    Try
                        eastPreviouslyAttackedSquare = Board(.FileIndex + 1, .RankIndex)
                    Catch ex As Exception
                        eastPreviouslyAttackedSquare = Nothing
                    End Try
                End With
            End If
            If westPreviouslyAttackedSquare IsNot Nothing Then westPreviouslyAttackedSquare.AttackedBy.Remove(move.ToSquare.Piece)
            If eastPreviouslyAttackedSquare IsNot Nothing Then eastPreviouslyAttackedSquare.AttackedBy.Remove(move.ToSquare.Piece)

            ' Add the pawn as an attacker to the squares it now attacks
            For Each attackingMove In move.ToSquare.Piece.GenerateAttackingMoves()
                attackingMove.ToSquare.AttackedBy.Add(move.ToSquare.Piece)
            Next
        ElseIf move.ToSquare.Piece.GetType = GetType(Knight) Or move.ToSquare.Piece.GetType = GetType(King) Then
            ' TODO: Optimise this method
            For Each square In Board
                square.AttackedBy.Remove(move.ToSquare.Piece)
            Next
            For Each attackingMove In move.ToSquare.Piece.GenerateAttackingMoves
                attackingMove.ToSquare.AttackedBy.Add(move.ToSquare.Piece)
            Next
        End If

        With move.FromSquare
            ' North
            RecalcSquaresAttackedBy(.RaycastNorth(Me, True), GetType(Rook), GetType(Queen))
            ' South
            RecalcSquaresAttackedBy(.RaycastSouth(Me, True), GetType(Rook), GetType(Queen))
            ' East
            RecalcSquaresAttackedBy(.RaycastEast(Me, True), GetType(Rook), GetType(Queen))
            ' West
            RecalcSquaresAttackedBy(.RaycastWest(Me, True), GetType(Rook), GetType(Queen))

            ' Northeast
            RecalcSquaresAttackedBy(.RaycastNortheast(Me, True), GetType(Bishop), GetType(Queen))
            ' Northwest
            RecalcSquaresAttackedBy(.RaycastNorthwest(Me, True), GetType(Bishop), GetType(Queen))
            ' Southeast
            RecalcSquaresAttackedBy(.RaycastSoutheast(Me, True), GetType(Bishop), GetType(Queen))
            ' Southwest
            RecalcSquaresAttackedBy(.RaycastSouthwest(Me, True), GetType(Bishop), GetType(Queen))
        End With

        With move.ToSquare
            ' North
            RecalcSquaresAttackedBy(.RaycastNorth(Me, True), GetType(Rook), GetType(Queen))
            ' South
            RecalcSquaresAttackedBy(.RaycastSouth(Me, True), GetType(Rook), GetType(Queen))
            ' East
            RecalcSquaresAttackedBy(.RaycastEast(Me, True), GetType(Rook), GetType(Queen))
            ' West
            RecalcSquaresAttackedBy(.RaycastWest(Me, True), GetType(Rook), GetType(Queen))

            ' Northeast
            RecalcSquaresAttackedBy(.RaycastNortheast(Me, True), GetType(Bishop), GetType(Queen))
            ' Northwest
            RecalcSquaresAttackedBy(.RaycastNorthwest(Me, True), GetType(Bishop), GetType(Queen))
            ' Southeast
            RecalcSquaresAttackedBy(.RaycastSoutheast(Me, True), GetType(Bishop), GetType(Queen))
            ' Southwest
            RecalcSquaresAttackedBy(.RaycastSouthwest(Me, True), GetType(Bishop), GetType(Queen))
        End With
    End Sub

    Private Sub RecalcSquaresAttackedBy(squares As List(Of Square), ByVal ParamArray pieceTypes() As Type)
        If squares.Count = 0 Then Return
        If squares.Last.Piece IsNot Nothing Then
            Dim attackingPiece As Piece = squares.Last.Piece
            squares.ForEach(Sub(sq As Square) sq.AttackedBy.Remove(attackingPiece))
            If pieceTypes.Contains(attackingPiece.GetType) Then
                For Each move In attackingPiece.GenerateAttackingMoves
                    move.ToSquare.AttackedBy.Add(attackingPiece)
                Next
            End If
        End If
    End Sub

    Private Function GetSquareContents(square As Square) As Piece
        Return Board(square.FileIndex, square.RankIndex).Piece
    End Function

    Public Sub MakeMove(move As Move)
        If move Is Nothing Then Throw New ArgumentNullException("move")
        If move.FromSquare.Piece Is Nothing Then Throw New InvalidChessMoveException("Cannot move from a square that has no piece.")
        If (move.Flags And MoveFlag.Promotion) <> 0 AndAlso move.PromotionPiece = Promotion.None Then Throw New InvalidChessMoveException("The move has a promotion flag set but no promotion piece.")
        If Not move.FromSquare.Piece.GenerateLegalMoves.Contains(move) Then Throw New InvalidChessMoveException("Illegal move: " & move.ToString)

        If (move.Flags And MoveFlag.CastleKingside) <> 0 Then
            ' Kingside castling
            ' Move the rook to the left of the king
            With move.ToSquare
                Board(.FileIndex - 1, .RankIndex).Piece = GetSquareContents(Board(7, .RankIndex))
                Board(7, .RankIndex).Piece = Nothing
                Board(.FileIndex - 1, .RankIndex).Piece.MySquare = Board(.FileIndex - 1, .RankIndex)
                Board(.FileIndex - 1, .RankIndex).Piece.HasMoved = True
            End With
            ' Disallow castling
            If currentTurnValue = Color.White Then
                WhiteCastlingRights = CastlingRights.None
            Else
                BlackCastlingRights = CastlingRights.None
            End If
        ElseIf (move.Flags And MoveFlag.CastleQueenside) <> 0 Then
            ' Queenside castling
            ' Move the rook to the right of the king
            With move.ToSquare
                Board(.FileIndex + 1, .RankIndex).Piece = GetSquareContents(Board(0, .RankIndex))
                Board(0, .RankIndex).Piece = Nothing
                Board(.FileIndex + 1, .RankIndex).Piece.MySquare = Board(.FileIndex + 1, .RankIndex)
                Board(.FileIndex + 1, .RankIndex).Piece.HasMoved = True
            End With
            ' Disallow castling
            If currentTurnValue = Color.White Then
                WhiteCastlingRights = CastlingRights.None
            Else
                BlackCastlingRights = CastlingRights.None
            End If
        ElseIf move.FromSquare.Piece.GetType = GetType(King) Then
            ' Disallow castling
            If currentTurnValue = Color.White Then
                WhiteCastlingRights = CastlingRights.None
            Else
                BlackCastlingRights = CastlingRights.None
            End If
        ElseIf move.FromSquare.Piece.GetType = GetType(Rook) Then
            If move.FromSquare.FileIndex = 7 Then ' If the kingside rook moved
                ' Disable castling kingside
                If currentTurnValue = Color.White Then
                    WhiteCastlingRights = WhiteCastlingRights And CastlingRights.QueensideOnly
                Else
                    BlackCastlingRights = BlackCastlingRights And CastlingRights.QueensideOnly
                End If
            ElseIf move.FromSquare.FileIndex = 0 Then ' If the queenside rook moved
                ' Disable castling queenside
                If currentTurnValue = Color.White Then
                    WhiteCastlingRights = WhiteCastlingRights And CastlingRights.KingsideOnly
                Else
                    BlackCastlingRights = BlackCastlingRights And CastlingRights.KingsideOnly
                End If
            End If
        End If

        With move.ToSquare
            If .Piece IsNot Nothing Then
                .Piece.MySquare = Nothing
                .Piece.IsCaptured = True
            End If

            Board(.FileIndex, .RankIndex).Piece = GetSquareContents(move.FromSquare)
            Board(.FileIndex, .RankIndex).Piece.MySquare = Board(.FileIndex, .RankIndex)
            Board(.FileIndex, .RankIndex).Piece.HasMoved = True

            Dim prevRank As Integer = If(currentTurnValue = Color.White, -1, 1)
            ' If this move included a pawn push of two squares
            If (move.Flags And MoveFlag.PawnPushTwoSquares) <> 0 Then
                ' Mark the space behind this pawn as the 'en passant' square
                EnPassantSquare = Board(.FileIndex, .RankIndex + prevRank)
            Else
                EnPassantSquare = Nothing

                ' If this move included an en passant capture
                If (move.Flags And MoveFlag.EnPassantCapture) <> 0 Then
                    ' Remove the piece behind the moved pawn
                    Board(.FileIndex, .RankIndex + prevRank).Piece.IsCaptured = True
                    Board(.FileIndex, .RankIndex + prevRank).Piece.MySquare = Nothing
                    Board(.FileIndex, .RankIndex + prevRank).Piece = Nothing
                ElseIf (move.Flags And MoveFlag.Promotion) <> 0 Then
                    Select Case move.PromotionPiece
                        Case Promotion.Knight
                            Dim newKnight As New Knight With {.Owner = currentTurnValue, .Game = Me, .HasMoved = True, .MySquare = move.ToSquare}
                            Board(.FileIndex, .RankIndex).Piece = newKnight
                        Case Promotion.Bishop
                            Dim newBishop As New Bishop With {.Owner = currentTurnValue, .Game = Me, .HasMoved = True, .MySquare = move.ToSquare}
                            Board(.FileIndex, .RankIndex).Piece = newBishop
                        Case Promotion.Rook
                            Dim newRook As New Rook With {.Owner = currentTurnValue, .Game = Me, .HasMoved = True, .MySquare = move.ToSquare}
                            Board(.FileIndex, .RankIndex).Piece = newRook
                        Case Promotion.Queen
                            Dim newQueen As New Queen With {.Owner = currentTurnValue, .Game = Me, .HasMoved = True, .MySquare = move.ToSquare}
                            Board(.FileIndex, .RankIndex).Piece = newQueen
                    End Select
                End If
            End If
        End With

        Board(move.FromSquare.FileIndex, move.FromSquare.RankIndex).Piece = Nothing

        CurrentTurn = If(CurrentTurn = Color.White, Color.Black, Color.White)
        RecalculateAttackedSquares()  ' UpdateAttackedSquares(move)
        ' Update check status
        If CurrentTurn = Color.White Then
            Check = Not WhiteKing.HasNoEnemyAttackers(WhiteKing.MySquare)
        Else
            Check = Not BlackKing.HasNoEnemyAttackers(BlackKing.MySquare)
        End If

        ' TODO: Improve recalculation algorithm speed
        RecalculatePinnedPieces()
        Dim enemyCheckers As IEnumerable(Of Piece)
        If currentTurnValue = Color.White Then enemyCheckers = WhiteKing.EnemyAttackers(WhiteKing.MySquare) _
            Else enemyCheckers = BlackKing.EnemyAttackers(BlackKing.MySquare)

        enemyCheckers.ToList.ForEach(Sub(piece As Piece)
                                         piece.MySquare.ThreatToKing = True
                                     End Sub)

        ' Work out checkmate or stalemate
        If LegalMoves.Count = 0 Then
            Checkmate = Check
            Stalemate = Not Check
        End If
    End Sub

    Private Sub RecalculatePinnedPieces()
        For Each square In Board.Cast(Of Square)
            square.ThreatToKing = False
            If square.Piece IsNot Nothing Then square.Piece.PinDirection = 0
        Next
        Dim activeKing As King = If(CurrentTurn = Color.White, WhiteKing, BlackKing)
        ' Don't bother calculating pinned pieces if in check and there are multiple attackers on the king
        If Check AndAlso activeKing.EnemyAttackers(activeKing.MySquare).Count >= 2 Then Return

        ' Mark pinned pieces
        Dim raycastStraightSquares(), raycastDiagonalSquares() As List(Of Square)
        With activeKing.MySquare
            raycastStraightSquares = New List(Of Square)() {
                .RaycastNorth(Me),
                .RaycastEast(Me),
                .RaycastSouth(Me),
                .RaycastWest(Me)
            }
            raycastDiagonalSquares = New List(Of Square)() {
                .RaycastNortheast(Me),
                .RaycastNorthwest(Me),
                .RaycastSoutheast(Me),
                .RaycastSouthwest(Me)
            }
        End With

        For count = 0 To raycastStraightSquares.Length - 1
            Dim squares = raycastStraightSquares(count)
            Dim foundPiece As Piece = Nothing
            For i = 0 To squares.Count - 1
                Dim square As Square = squares(i)
                Dim thisPiece As Piece = square.Piece
                If thisPiece IsNot Nothing Then
                    If foundPiece IsNot Nothing AndAlso thisPiece.Owner = foundPiece.Owner Then Exit For
                    If thisPiece.OppositeOwner = CurrentTurn AndAlso (thisPiece.GetType = GetType(Queen) OrElse thisPiece.GetType = GetType(Rook)) Then
                        If foundPiece IsNot Nothing Then
                            foundPiece.PinDirection = {DirectionFlag.North,
                                                       DirectionFlag.East,
                                                       DirectionFlag.South,
                                                       DirectionFlag.West}(count)
                            Exit For
                        ElseIf Check Then
                            For j = 0 To i
                                squares(j).ThreatToKing = True
                            Next
                            Exit For
                        Else
                            Exit For
                        End If
                    ElseIf foundPiece Is Nothing Then
                        foundPiece = thisPiece
                    Else
                        Exit For
                    End If
                End If
            Next
        Next

        For count = 0 To raycastDiagonalSquares.Length - 1
            Dim squares = raycastDiagonalSquares(count)
            Dim foundPiece As Piece = Nothing
            For i = 0 To squares.Count - 1
                Dim square As Square = squares(i)
                Dim thisPiece As Piece = square.Piece
                If thisPiece IsNot Nothing Then
                    If foundPiece IsNot Nothing AndAlso thisPiece.Owner = foundPiece.Owner Then Exit For
                    If thisPiece.OppositeOwner = CurrentTurn AndAlso (thisPiece.GetType = GetType(Queen) OrElse thisPiece.GetType = GetType(Bishop)) Then
                        If foundPiece IsNot Nothing Then
                            foundPiece.PinDirection = {DirectionFlag.Northeast,
                                                       DirectionFlag.Northwest,
                                                       DirectionFlag.Southeast,
                                                       DirectionFlag.Southwest}(count)
                            Exit For
                        ElseIf Check Then
                            For j = 0 To i
                                squares(j).ThreatToKing = True
                            Next
                            Exit For
                        Else
                            Exit For
                        End If
                    Else
                        foundPiece = thisPiece
                    End If
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Initialize a new chess game with the default position.
    ''' </summary>
    Public Sub New()
        Me.New(ChessGame.StartingPosition)
    End Sub

    ''' <summary>
    ''' Initialize a new chess game and load either a position as FEN or an entire game as PGN.
    ''' </summary>
    ''' <param name="data">The FEN or PGN to use.</param>
    Public Sub New(ByVal data As String)
        ' TODO: Implement New ChessGame(data As String)
        Throw New NotImplementedException
    End Sub

    ''' <summary>
    ''' Initialize a new chess game from a position using a Dictionary(Of String, Char).
    ''' </summary>
    ''' <param name="pieceDict">A dictionary mapping a position to a FEN character, e.g. "e1", "K".</param>
    Public Sub New(ByVal pieceDict As Dictionary(Of String, Char))
        For i = 0 To 7
            For j = 0 To 7
                Board(j, i) = New Square(j, i)
                Try
                    Board(j, i).Piece = Piece.FromFENChar(pieceDict(Board(j, i).ToString), Me)
                    Board(j, i).Piece.MySquare = Board(j, i)
                    If Board(j, i).Piece.GetType = GetType(King) Then
                        If Board(j, i).Piece.Owner = Color.White Then
                            If WhiteKing Is Nothing Then
                                WhiteKing = CType(Board(j, i).Piece, King)
                            Else
                                Throw New InvalidChessPositionException("Only one white king is allowed.")
                            End If
                        Else
                            If BlackKing Is Nothing Then
                                BlackKing = CType(Board(j, i).Piece, King)
                            Else
                                Throw New InvalidChessPositionException("Only one black king is allowed.")
                            End If
                        End If
                    End If
                Catch ex As KeyNotFoundException
                End Try
            Next
        Next
        WhiteCastlingRights = CastlingRights.Both
        BlackCastlingRights = CastlingRights.Both
        currentTurnValue = Color.White
        RecalculateAttackedSquares()
    End Sub

End Class

