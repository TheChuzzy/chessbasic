﻿Imports System.Runtime.Serialization
Imports System.Security

''' <summary>
''' The exception thrown by the ChessBasic library.
''' </summary>
<Serializable>
Public Class ChessException
    Inherits Exception

    Public Sub New()
        Me.New("An exception occured in ChessBasic.")
    End Sub

    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(message As String, inner As Exception)
        MyBase.New(message, inner)
    End Sub

    <SecuritySafeCritical()>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class