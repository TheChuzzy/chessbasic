﻿Option Strict On
Public Class Move
    Implements IEquatable(Of Move)
    Public ReadOnly FromSquare, ToSquare As Square, Flags As Integer, PromotionPiece As Promotion

    Public Sub New(oldMove As Move, promotion As Promotion)
        Me.FromSquare = oldMove.FromSquare
        Me.ToSquare = oldMove.ToSquare
        Me.Flags = oldMove.Flags
        Me.PromotionPiece = promotion
    End Sub

    Public Sub New(fromSquare As Square, toSquare As Square, flags As Integer, promotion As Promotion)
        Me.FromSquare = fromSquare
        Me.ToSquare = toSquare
        Me.Flags = flags
        PromotionPiece = promotion
    End Sub

    Public Sub New(fromSquare As Square, toSquare As Square, flags As Integer)
        Me.New(fromSquare, toSquare, flags, Promotion.None)
    End Sub

    Public Sub New(fromSquare As Square, toSquare As Square)
        Me.New(fromSquare, toSquare, 0)
    End Sub

    Public Overrides Function ToString() As String
        Return FromSquare.ToString & ToSquare.ToString
    End Function

    Function Equals(other As Move) As Boolean Implements IEquatable(Of Move).Equals
        Return (FromSquare.Equals(other.FromSquare)) AndAlso (ToSquare.Equals(other.ToSquare)) AndAlso (Flags.Equals(other.Flags))
    End Function
End Class

