﻿Option Strict On

Public Class Square
    ''' <summary>
    ''' A list of pieces that are currently attacking this square.
    ''' </summary>
    Public AttackedBy As New HashSet(Of Piece)
    ''' <summary>
    ''' Whether a piece can move here to stop check.
    ''' </summary>
    Public ThreatToKing As Boolean
    Public Piece As Piece

    Private fileIndexValue As Integer
    Public ReadOnly Property FileIndex As Integer
        Get
            Return fileIndexValue
        End Get
    End Property
    Private rankIndexValue As Integer
    Public ReadOnly Property RankIndex As Integer
        Get
            Return rankIndexValue
        End Get
    End Property

    Public ReadOnly Property FileName As Char
        Get
            Return "abcdefgh"(FileIndex)
        End Get
    End Property

    Public ReadOnly Property RankName As Char
        Get
            Return "12345678"(RankIndex)
        End Get
    End Property

    ''' <summary>
    ''' Whether this square is lightly-colored.
    ''' </summary>
    Public ReadOnly Property IsLightSquare As Boolean
        Get
            Return 8 * rankIndexValue + fileIndexValue Mod 2 = 1
        End Get
    End Property

    ''' <summary>
    ''' Casts a ray from this square's position to the north and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastNorth(game As ChessGame, Optional stopAtPiece As Boolean = False ) As List(Of Square)
        Dim squares As New List(Of Square)

        For rank = RankIndex + 1 To 7
            squares.Add(game.Board(FileIndex, rank))
            If stopAtPiece AndAlso game.Board(FileIndex, rank) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the south and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastSouth(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For rank = RankIndex - 1 To 0 Step -1
            squares.Add(game.Board(FileIndex, rank))
            If stopAtPiece AndAlso game.Board(FileIndex, rank) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the east and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastEast(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For file = FileIndex + 1 To 7
            squares.Add(game.Board(file, RankIndex))
            If stopAtPiece AndAlso game.Board(file, RankIndex) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the west and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastWest(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For file = FileIndex - 1 To 0 Step -1
            squares.Add(game.Board(file, RankIndex))
            If stopAtPiece AndAlso game.Board(file, RankIndex) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the northeast and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastNortheast(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For i = 1 To Math.Min(7 - FileIndex, 7 - RankIndex)
            squares.Add(game.Board(FileIndex + i, RankIndex + i))
            If stopAtPiece AndAlso game.Board(FileIndex + i, RankIndex + i) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the southwest and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastSouthwest(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For i = 1 To Math.Min(FileIndex, RankIndex)
            squares.Add(game.Board(FileIndex - i, RankIndex - i))
            If stopAtPiece AndAlso game.Board(FileIndex - i, RankIndex - i) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the northwest and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastNorthwest(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For i = 1 To Math.Min(FileIndex, 7 - RankIndex)
            squares.Add(game.Board(FileIndex - i, RankIndex + i))
            If stopAtPiece AndAlso game.Board(FileIndex - i, RankIndex + i) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    ''' <summary>
    ''' Casts a ray from this square's position to the southeast and returns an ordered list of the squares.
    ''' </summary>
    ''' <param name="game">The game to use.</param>
    ''' <param name="stopAtPiece">Whether to stop as soon as a piece is hit.</param>
    ''' <returns>A List(Of Square).</returns>
    Public Function RaycastSoutheast(game As ChessGame, Optional stopAtPiece As Boolean = False) As List(Of Square)
        Dim squares As New List(Of Square)

        For i = 1 To Math.Min(7 - FileIndex, RankIndex)
            squares.Add(game.Board(FileIndex + i, RankIndex - i))
            If stopAtPiece AndAlso game.Board(FileIndex + i, RankIndex - i) IsNot Nothing Then
                Return squares
            End If
        Next

        Return squares
    End Function

    Public Overrides Function ToString() As String
        Return FileName + RankName
    End Function

    Public Sub New(file As Integer, rank As Integer)
        fileIndexValue = file
        rankIndexValue = rank
    End Sub
End Class
