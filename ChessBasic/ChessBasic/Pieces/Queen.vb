﻿Option Strict On

Imports System.Collections.ObjectModel
Public Class Queen
    Inherits Piece

    Public Overrides Function GenerateAttackingMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)

        ' Generate moves to the east
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastEast(Game)))

        ' Generate moves to the west
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastWest(Game)))

        ' Generate moves to the north
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastNorth(Game)))

        ' Generate moves to the south
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastSouth(Game)))

        ' Generate moves to the northeast
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastNortheast(Game)))

        ' Generate moves to the southeast
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastSoutheast(Game)))

        ' Generate moves to the southwest
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastSouthwest(Game)))

        ' Generate moves to the northwest
        moves.AddRange(SliderGenerateMoves(MySquare.RaycastNorthwest(Game)))

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function

    Public Overrides Function GenerateLegalMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)
        ' Cannot move this piece if in double check
        If Game.Check AndAlso EnemyAttackers(MyKing.MySquare).Count > 1 Then Return New ReadOnlyCollection(Of Move)(moves)

        ' If this piece is not pinned diagonally
        If (PinDirection And (DirectionFlag.Northeast Or DirectionFlag.Northwest Or DirectionFlag.Southeast Or DirectionFlag.Southwest)) = 0 Then
            ' If this piece is not pinned to the north or south
            If (PinDirection And (DirectionFlag.North Or DirectionFlag.South)) = 0 Then
                ' This rook can move to the west and east
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastEast(Game), False))
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastWest(Game), False))
            End If

            ' If this piece is not pinned to the west or east
            If (PinDirection And (DirectionFlag.West Or DirectionFlag.East)) = 0 Then
                ' This rook can move to the north and south
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastNorth(Game), False))
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastSouth(Game), False))
            End If
        End If

        ' If this piece is not pinned horizontally or vertically
        If (PinDirection And (DirectionFlag.North Or DirectionFlag.East Or DirectionFlag.West Or DirectionFlag.South)) = 0 Then
            ' If this piece is not pinned to the northeast or southwest
            If (PinDirection And (DirectionFlag.Northeast Or DirectionFlag.Southwest)) = 0 Then
                ' This bishop can move to the northwest and southeast
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastNorthwest(Game), False))
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastSoutheast(Game), False))
            End If

            ' If this piece is not pinned to the northwest or southeast
            If (PinDirection And (DirectionFlag.Northwest Or DirectionFlag.Southeast)) = 0 Then
                ' This bishop can move to the northeast and southwest
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastNortheast(Game), False))
                moves.AddRange(SliderGenerateMoves(MySquare.RaycastSouthwest(Game), False))
            End If
        End If

        If Game.Check Then
            ' Only permit moves which block or capture the checking piece
            moves = moves.Where(Function(move As Move)
                                    Return move.ToSquare.ThreatToKing
                                End Function).ToList
        End If

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function
End Class

