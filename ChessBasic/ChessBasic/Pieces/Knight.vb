﻿Option Strict On

Imports System.Collections.ObjectModel
Public Class Knight
    Inherits Piece

    Public Overrides ReadOnly Property FENChar As Char
        Get
            Return If(Owner = Color.White, "N"c, "n"c)
        End Get
    End Property

    Public Overrides Function GenerateAttackingMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)

        ' This array contains all 8 possible knight moves going clockwise, recorded as offsets.
        Dim positionOffsets As Integer()() = {
            New Integer() {-2, 1},
            New Integer() {-1, 2},
            New Integer() {1, 2},
            New Integer() {2, 1},
            New Integer() {2, -1},
            New Integer() {1, -2},
            New Integer() {-1, -2},
            New Integer() {-2, -1}
        }

        For Each offset In positionOffsets
            Try
                Dim currentSquare As Square = Game.Board(MySquare.FileIndex + offset(0), MySquare.RankIndex + offset(1))

                If currentSquare.Piece Is Nothing Then
                    moves.Add(New Move(MySquare, currentSquare))
                Else
                    moves.Add(New Move(MySquare, currentSquare, MoveFlag.Capture))
                End If
            Catch ex As IndexOutOfRangeException
            End Try
        Next

        If Game.Check Then
            ' Only permit moves which block or capture the checking piece
            moves = moves.Where(Function(move As Move)
                                    Return move.ToSquare.ThreatToKing
                                End Function).ToList
        End If

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function

    Public Overrides Function GenerateLegalMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)
        ' Cannot move this piece if in double check
        If Game.Check AndAlso EnemyAttackers(MyKing.MySquare).Count > 1 Then Return New ReadOnlyCollection(Of Move)(moves)

        ' A knight that is pinned to the king cannot move.
        If PinDirection <> DirectionFlag.None Then Return New ReadOnlyCollection(Of Move)(moves)

        ' Otherwise, the moves that can be made are the same as GenerateAttackingMoves
        ' (except for the moves where the knight takes his own pieces).

        For Each move In GenerateAttackingMoves()
            If move.ToSquare.Piece Is Nothing OrElse move.ToSquare.Piece.OppositeOwner = Owner Then moves.Add(move)
        Next

        If Game.Check Then
            ' Only permit moves which block or capture the checking piece
            Dim enemyChecker As Piece = EnemyAttackers(MyKing.MySquare)(0)
            moves = (From move In moves
                     Where move.ToSquare.AttackedBy.Contains(enemyChecker) OrElse
                     move.ToSquare.Piece IsNot Nothing AndAlso move.ToSquare.Piece.Equals(enemyChecker)
                     Select move).ToList
        End If

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function
End Class

