﻿Option Strict On

Imports System.Collections.ObjectModel
Public Class King
    Inherits Piece

    Public Overrides Function GenerateAttackingMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)

        ' Contains all 8 possible king moves going clockwise, recorded as offsets.
        Dim positionOffsets As Integer()() = {
            New Integer() {-1, 1},
            New Integer() {0, 1},
            New Integer() {1, 1},
            New Integer() {1, 0},
            New Integer() {1, -1},
            New Integer() {0, -1},
            New Integer() {-1, -1},
            New Integer() {-1, 0}
        }

        For Each offset In positionOffsets
            Try
                Dim currentSquare As Square = Game.Board(MySquare.FileIndex + offset(0), MySquare.RankIndex + offset(1))

                If currentSquare.Piece Is Nothing Then
                    moves.Add(New Move(MySquare, currentSquare))
                Else
                    moves.Add(New Move(MySquare, currentSquare, MoveFlag.Capture))
                End If
            Catch ex As IndexOutOfRangeException
            End Try
        Next

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function

    Friend Function HasNoEnemyAttackers(square As Square) As Boolean
        Return (From piece In square.AttackedBy Where piece.OppositeOwner = Owner Select piece).Count = 0
    End Function

    Public Overrides Function GenerateLegalMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)

        If Not HasMoved AndAlso Not Game.Check Then
            If If(Owner = Color.White, Game.WhiteCastlingRights, Game.BlackCastlingRights).HasFlag(CastlingRights.KingsideOnly) Then
                Dim oneSquareEast As Square = Game.Board(MySquare.FileIndex + 1, MySquare.RankIndex)
                Dim twoSquaresEast As Square = Game.Board(MySquare.FileIndex + 2, MySquare.RankIndex)

                If oneSquareEast.Piece Is Nothing And twoSquaresEast.Piece Is Nothing And
                    HasNoEnemyAttackers(oneSquareEast) And HasNoEnemyAttackers(twoSquaresEast) Then
                    moves.Add(New Move(MySquare, twoSquaresEast, MoveFlag.CastleKingside))
                End If
            End If
            If If(Owner = Color.White, Game.WhiteCastlingRights, Game.BlackCastlingRights).HasFlag(CastlingRights.QueensideOnly) Then
                Dim oneSquareWest As Square = Game.Board(MySquare.FileIndex - 1, MySquare.RankIndex)
                Dim twoSquaresWest As Square = Game.Board(MySquare.FileIndex - 2, MySquare.RankIndex)
                Dim threeSquaresWest As Square = Game.Board(MySquare.FileIndex - 3, MySquare.RankIndex)

                If oneSquareWest.Piece Is Nothing And twoSquaresWest.Piece Is Nothing And threeSquaresWest.Piece Is Nothing And
                    HasNoEnemyAttackers(oneSquareWest) And HasNoEnemyAttackers(twoSquaresWest) Then
                    moves.Add(New Move(MySquare, twoSquaresWest, MoveFlag.CastleQueenside))
                End If
            End If
        End If

        For Each move In GenerateAttackingMoves()
            ' King cannot capture his own pieces
            If move.ToSquare.Piece IsNot Nothing AndAlso move.ToSquare.Piece.Owner = Owner Then Continue For

            ' King cannot move to a square attacked by the enemy
            If Not HasNoEnemyAttackers(move.ToSquare) Then Continue For

            moves.Add(move)
        Next

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function
End Class

