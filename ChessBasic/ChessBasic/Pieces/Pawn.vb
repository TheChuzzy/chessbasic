﻿Imports System.Collections.ObjectModel
Imports ChessBasic

Public Class Pawn
    Inherits Piece

    Public Overrides Function GenerateAttackingMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)

        ' The rank (row) that is in front of this pawn.
        Dim nextRank As Integer = If(Owner = Color.White, 1, -1)

        If MySquare.FileIndex > 0 Then moves.Add(New Move(MySquare, Game.Board(MySquare.FileIndex - 1, MySquare.RankIndex + nextRank)))
        If MySquare.FileIndex < 7 Then moves.Add(New Move(MySquare, Game.Board(MySquare.FileIndex + 1, MySquare.RankIndex + nextRank)))

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function

    Public Overrides Function GenerateLegalMoves() As ReadOnlyCollection(Of Move)
        Dim moves As New List(Of Move)
        ' Cannot move this piece if in double check
        If Game.Check AndAlso EnemyAttackers(MyKing.MySquare).Count > 1 Then Return New ReadOnlyCollection(Of Move)(moves)

        ' If this pawn is pinned to the northeast
        Select Case PinDirection
            Case DirectionFlag.East, DirectionFlag.West
                ' If this pawn is pinned to the king horizontally, it cannot move.
                Return New ReadOnlyCollection(Of Move)(moves)

            Case DirectionFlag.Northeast, DirectionFlag.Southwest
                Dim move As Move = ValidatePawnCapture(DirectionFlag.East)
                If move Is Nothing Then
                    Return New ReadOnlyCollection(Of Move)(moves)
                Else
                    moves.Add(move)
                    Return New ReadOnlyCollection(Of Move)(moves)
                End If

            Case DirectionFlag.Northwest, DirectionFlag.Southeast
                Dim move As Move = ValidatePawnCapture(DirectionFlag.West)
                If move Is Nothing Then
                    Return New ReadOnlyCollection(Of Move)(moves)
                Else
                    moves.Add(move)
                    Return New ReadOnlyCollection(Of Move)(moves)
                End If

            Case DirectionFlag.North, DirectionFlag.South
                If (Owner = Color.White And MySquare.RankIndex = 1) Or
                   (Owner = Color.Black And MySquare.RankIndex = 6) Then
                    Dim twoSquareMove As Move = ValidatePawnPush(True)

                    If twoSquareMove IsNot Nothing Then
                        moves.Add(twoSquareMove)
                    End If
                End If

                Dim oneSquareMove As Move = ValidatePawnPush(False)
                If oneSquareMove IsNot Nothing Then
                    moves.Add(oneSquareMove)
                End If

            Case Else
                If (Owner = Color.White And MySquare.RankIndex = 1) Or
                   (Owner = Color.Black And MySquare.RankIndex = 6) Then
                    Dim twoSquareMove As Move = ValidatePawnPush(True)

                    If twoSquareMove IsNot Nothing Then
                        moves.Add(twoSquareMove)
                    End If
                End If

                Dim oneSquareMove As Move = ValidatePawnPush(False)
                If oneSquareMove IsNot Nothing Then
                    moves.Add(oneSquareMove)
                End If

                Dim captureEast As Move = ValidatePawnCapture(DirectionFlag.East)
                If captureEast IsNot Nothing Then
                    moves.Add(captureEast)
                End If

                Dim captureWest As Move = ValidatePawnCapture(DirectionFlag.West)
                If captureWest IsNot Nothing Then
                    moves.Add(captureWest)
                End If
        End Select

        If Game.Check Then
            ' Only permit moves which block or capture the checking piece
            moves = moves.Where(Function(move As Move)
                                    Return move.ToSquare.ThreatToKing
                                End Function).ToList
        End If

        Return New ReadOnlyCollection(Of Move)(moves)
    End Function

    ''' <summary>
    ''' Returns a valid pawn capture in the specified direction, or Nothing if there is no valid move.
    ''' </summary>
    ''' <param name="direction">The capture direction to check.</param>
    Private Function ValidatePawnCapture(ByVal direction As DirectionFlag) As Move
        If direction And (DirectionFlag.East Or DirectionFlag.West) = 0 Then Throw New ArgumentException("direction must be East or West.")
        Dim targetSquare As Square

        Try
            If Owner = Color.White Then
                If direction = DirectionFlag.West Then
                    targetSquare = Game.Board(MySquare.FileIndex - 1, MySquare.RankIndex + 1)
                Else
                    targetSquare = Game.Board(MySquare.FileIndex + 1, MySquare.RankIndex + 1)
                End If
            Else
                If direction = DirectionFlag.West Then
                    targetSquare = Game.Board(MySquare.FileIndex - 1, MySquare.RankIndex - 1)
                Else
                    targetSquare = Game.Board(MySquare.FileIndex + 1, MySquare.RankIndex - 1)
                End If
            End If
        Catch ex As IndexOutOfRangeException
            Return Nothing
        End Try

        If targetSquare.Piece IsNot Nothing AndAlso targetSquare.Piece.OppositeOwner = Owner Then
            Return New Move(MySquare, targetSquare, MoveFlag.Capture Or If(targetSquare.RankIndex = 0 Or targetSquare.RankIndex = 7, MoveFlag.Promotion, MoveFlag.None))
        ElseIf Game.EnPassantSquare IsNot Nothing AndAlso Game.EnPassantSquare.Equals(targetSquare) Then
            ' Verify that en passant would not leave the king in check

            ' En passant would only leave the king in check if it is on the same rank as the pawn
            If MySquare.RankIndex = MyKing.MySquare.RankIndex Then
                Dim squaresToCheck As List(Of Square)
                ' Check that there are no pieces between the pawn and its king
                If MySquare.FileIndex > MyKing.MySquare.FileIndex Then
                    squaresToCheck = MySquare.RaycastWest(Game, True)
                Else
                    squaresToCheck = MySquare.RaycastEast(Game, True)
                End If
                If squaresToCheck.Last.Piece.GetType Is GetType(King) Then

                    ' If this pawn is to the right of the king
                    If MySquare.FileIndex > MyKing.MySquare.FileIndex Then
                        ' Check squares to the right
                        squaresToCheck = MySquare.RaycastEast(Game)
                        If direction = DirectionFlag.East Then
                            ' Remove the square with the pawn on it
                            squaresToCheck.RemoveAt(0)
                        End If
                    Else
                        ' Check squares to the left
                        squaresToCheck = MySquare.RaycastWest(Game)
                        If direction = DirectionFlag.West Then
                            ' Remove the square with the pawn on it
                            squaresToCheck.RemoveAt(0)
                        End If
                    End If

                    For Each square In squaresToCheck
                        ' If there is a piece
                        If square.Piece IsNot Nothing Then
                            ' If it is an enemy piece
                            If square.Piece.OppositeOwner = Owner Then
                                ' If it is an enemy rook or queen
                                If square.Piece.GetType = GetType(Rook) OrElse square.Piece.GetType = GetType(Queen) Then
                                    ' Capturing en passant would result in discovered check - illegal move
                                    Return Nothing
                                Else
                                    Exit For
                                End If
                            Else
                                Exit For
                            End If
                        End If
                    Next
                End If

                Return New Move(MySquare, targetSquare, MoveFlag.EnPassantCapture Or If(targetSquare.RankIndex = 0 Or targetSquare.RankIndex = 7, MoveFlag.Promotion, MoveFlag.None))
            Else
                Return New Move(MySquare, targetSquare, MoveFlag.EnPassantCapture Or If(targetSquare.RankIndex = 0 Or targetSquare.RankIndex = 7, MoveFlag.Promotion, MoveFlag.None))
            End If
            Else
                Return Nothing
            End If
    End Function

    ''' <summary>
    ''' Returns a valid pawn push as a Move, or Nothing if there is no valid move.
    ''' </summary>
    ''' <param name="twoSquares">Whether to check for a two square push instead of just one.</param>
    Private Function ValidatePawnPush(ByVal twoSquares As Boolean) As Move
        ' The rank (row) that is in front of this pawn.
        Dim nextRank As Integer = If(Owner = Color.White, 1, -1)
        Dim targetSquare As Square = Game.Board(MySquare.FileIndex, MySquare.RankIndex + nextRank)

        If targetSquare.Piece IsNot Nothing Then
            Return Nothing
        Else
            ValidatePawnPush = New Move(MySquare, targetSquare, If(targetSquare.RankIndex = 0 Or targetSquare.RankIndex = 7, MoveFlag.Promotion, MoveFlag.None))
        End If

        If twoSquares Then
            targetSquare = Game.Board(MySquare.FileIndex, MySquare.RankIndex + nextRank * 2)
            If targetSquare.Piece IsNot Nothing Then
                Return Nothing
            Else
                Return New Move(MySquare, targetSquare, MoveFlag.PawnPushTwoSquares)
            End If
        Else
            Return ValidatePawnPush
        End If
    End Function
End Class
